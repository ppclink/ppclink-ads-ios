//
//  VDUIRemoveAdsViewController.m
//  VDFramework_sample
//
//  Created by Do Lam on 4/9/15.
//  Copyright (c) 2015 Do Lam. All rights reserved.
//

#import "VDUIRemoveAdsViewController.h"
#import "VDConfigNotification.h"
#import "MPMediationAdController.h"
#import "VDUtilities.h"

@interface VDUIRemoveAdsViewController ()
{
    NSMutableArray *listApps;
}
@end

@implementation VDUIRemoveAdsViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        self.title = @"Remove Ads";
        
        // Init notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotifyDidFinishDownloadAdImages:) name:kNotifyDidFinishDownloadAdImages object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNotifyDidLoadNewConfig:) name:kNotifyDidLoadNewConfigNotification object:nil];
        

        listApps = [[NSMutableArray alloc] initWithArray:[g_vdConfigNotification getListOfAppDownloadForReward]];
        
    }
    return self;
}

-(void)updateAdFreeHours
{
    [labelNumberOfAdFreeHours setText:[NSString stringWithFormat:@"Number of Ad-free hours remaining: %d", [g_vdConfigNotification getNumberOfAdFreeHoursRemain]]];
}

- (void)onNotifyDidLoadNewConfig:(NSNotification *)notify
{
    [listApps setArray:[g_vdConfigNotification getListOfAppDownloadForReward]];
    [_tableView reloadData];
}

- (void) loadView
{
    [super loadView];
    
    CGRect mainFrame = self.view.frame;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mainFrame.size.width, mainFrame.size.height-50) style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    labelNumberOfAdFreeHours = [[UILabel alloc] initWithFrame:CGRectMake(0, mainFrame.size.height-50, mainFrame.size.width, 40)];
    [labelNumberOfAdFreeHours setText:[NSString stringWithFormat:@"Number of Ad-free hours remaining: %d", [g_vdConfigNotification getNumberOfAdFreeHoursRemain]]];
    labelNumberOfAdFreeHours.backgroundColor = [UIColor greenColor];
    [self.view addSubview:labelNumberOfAdFreeHours];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [labelNumberOfAdFreeHours setText:[NSString stringWithFormat:@"Number of Ad-free hours remaining: %d", [g_vdConfigNotification getNumberOfAdFreeHoursRemain]]];
    
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return [listApps count];
    else
        return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *strCellTag = @"CellTag";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCellTag];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:strCellTag];
    }
    
    if (indexPath.section == 0)
    {
        NSDictionary* dictCurAppInfo = [listApps objectAtIndex:indexPath.row];
        cell.textLabel.text = [dictCurAppInfo objectForKey:kNotification_Title];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d hours Ad Free", [[dictCurAppInfo objectForKey:kNotification_NumberOfAdFreeHours] intValue]];
    }
    else if (indexPath.section == 1)
    {
        switch (indexPath.row)
        {
            case 0:
                cell.textLabel.text = @"View Ads for reward";
                break;
        
            case 1:
                cell.textLabel.text = @"Buy Pro Version";
                break;

            default:
                break;
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Did select row %d",indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        NSDictionary* dictCurAppInfo = [listApps objectAtIndex:indexPath.row];
        NSString *sNotificationID = [dictCurAppInfo objectForKey:kNotification_ID];
        [g_vdConfigNotification userJustClickTryNowWithNotificationId:sNotificationID forReward:YES];
        
        NSString *sURL = [dictCurAppInfo objectForKey:kNotification_URL];
        sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"Open URL: %@", sURL);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sURL]];
    }
    else if (indexPath.section == 1)
    {
        switch (indexPath.row)
        {
           
            case 0:
            {
                 NSDictionary* dictCurAppInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:6],kNotification_NumberOfAdFreeHours, nil];
                // Goi ham hien quang cao o day
                if ([[MPMediationAdController sharedManager] isInterstitialAdAvailable:INTERSTITIAL_AD_TYPE_ALL])
                    [[MPMediationAdController sharedManager] showInterstitialAd:INTERSTITIAL_AD_TYPE_ALL withBonusInfo:dictCurAppInfo];
                break;
            }
        
            case 1:
                // Go to Buy Pro Version;
                break;
                
            default:
                break;
        }
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    _tableView.delegate = nil;
    
    [super dealloc];
}
*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
