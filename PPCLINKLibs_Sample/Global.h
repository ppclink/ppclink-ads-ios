//
//  Global.h
//  Smartkids
//
//  Created by minhbn on 7/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#ifndef __GLOBAL_H_
#define __GLOBAL_H_

#import <Foundation/Foundation.h>
#import "VDMailComposer.h"
//#import "common_dstring.h"
#import "VDUtilities.h"
//#import "CoreLVN.h"
//#import "DateCalcDefine.h"
//#import "DateTimerEngine.h"
//#import "NSData+Encryption.h"
//#import "Constanst.h"

//#import "GAI.h"
//#import "GAIFields.h"
//#import "GAITracker.h"
//#import "GAIDictionaryBuilder.h"

//#import "UIViewController+LV.h"


#define UIColorFromRGB(r, g, b)     [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0f]

#define DeepLinkKeyScreen @"screen"
#define DeepLinkKeyEventType @"event_type"
#define DeepLinkKeyEventInfo @"event_info"
#define DeepLinkKeyTabIndex @"tab_index"
#define DeepLinkKeyPersonName @"person_name"
#define DeepLinkKeyPersonBirthday @"person_birthday"
#define DeepLinkKeyDanhNgonCategoryId @"danh_ngon_cate_id"
#define DeepLinkKeyDanhNgonId @"danh_ngon_id"
#define DeepLinkKeyVanHoaVietItemId @"van_hoa_viet_item_id"
#define DeepLinkKeyDate @"date"
#define DeepLinkKeyDanhNgonAuthor @"danh_ngon_author"
#define DeepLinkKeyDanhNgonContent @"danh_ngon_content"
#define DeepLinkKeyVanHoaVietCategoryId @"van_hoa_viet_cate_id"
#define DeepLinkKeyThiepMungCategoryId @"thiep_mung_cate_id"
#define DeepLinkKeyThiepMungPhotoId @"thiep_mung_photo_id"
#define DeepLinkKeyTuViCategoryId @"tuvi_cate_id"


#define DeepLinkScreenNameLichNgay @"lich_ngay"
#define DeepLinkScreenNameLichNgayChiTietNgay @"lich_ngay:chi_tiet_ngay"
#define DeepLinkScreenNameLichNgayTimKiem @"lich_ngay:search"
#define DeepLinkScreenNameLichThang @"lich_thang"
#define DeepLinkScreenNameSuKien @"su_kien"
#define DeepLinkScreenNameSuKienChiTiet @"su_kien:chi_tiet"
#define DeepLinkScreenNameCungHoangDao @"kham_pha:cung_hoang_dao"
#define DeepLinkScreenNameNhipSinhHoc @"kham_pha:nhip_sinh_hoc"
#define DeepLinkScreenNameDemNguocXuoi @"kham_pha:dem_nguoc_xuoi"
#define DeepLinkScreenNameThiepMung @"kham_pha:thiep_mung"
#define DeepLinkScreenNameTuVi @"kham_pha:tu_vi"
#define DeepLinkScreenNameDanhNgon @"kham_pha:danh_ngon"
#define DeepLinkScreenNameDanhNgonChiTiet @"kham_pha:danh_ngon_chi_tiet"
#define DeepLinkScreenNamePhongTuc @"kham_pha:van_hoa_viet:phong_tuc"
#define DeepLinkScreenNameLeHoi @"kham_pha:van_hoa_viet:le_hoi"
#define DeepLinkScreenNameTroChoi @"kham_pha:van_hoa_viet:tro_choi"
#define DeepLinkScreenNameDongDao @"kham_pha:van_hoa_viet:dong_dao"
#define DeepLinkScreenNameHatRu @"kham_pha:van_hoa_viet:hat_ru"
#define DeepLinkScreenNameVanKhan @"kham_pha:van_hoa_viet:van_khan"
#define DeepLinkScreenNameTruyenCuoi @"kham_pha:van_hoa_viet:truyen_cuoi"
#define DeepLinkScreenNameSettings @"settings"
#define DeepLinkValueDateToday @"today"
#define DeepLinkScreenNameLaBan @"kham_pha:la_ban"
#define DeepLinkScreenNameThuocLoBan @"kham_pha:thuoc_lo_ban"
#define DeepLinkScreenNameHome @"home"



#define         GoogleAnalytic_KEY  @"UA-54111739-8"     //ppclink
//#define		GoogleAnalytic_KEY  @"UA-66240718-1"    //test

#define		MAX_LEN         255
#define		MAX_LINE        1024
#define     MAX_BUF         512

#define MAX_CANCHI  4
#define		MAX_ITEM_IMAGE_BGR	33

#define appAdIconDirectoryPath  [appDataDirectoryPath stringByAppendingString:@"/adIconMoreApps"]
//Notification
#define kNotifyUpdateSettingChanged            @"NotifyUpdateSettingChanged"
#define kNotifyChangeSelectDate                 @"NotifyChangeSelectDate"
#define kNotifyUpdateWeatherSuccess                 @"NotifyUpdateWeatherSuccess"

//#define kSwitchImage	@"SwitchImage"
//#define kSwitchImagePhoto	@"ChangeImagePhoto"
//
#define SETTING_MODETIME_KEY            @"ModeTime"
#define SETTING_ONOFFQUOTION_KEY        @"OnOffQuotion"
#define SETTING_MODEISLOCAL_KEY         @"MODEISLOCAL"
#define SETTING_PHOTO_KEY               @"PhotoKey"
#define NAMEBGR_KEY                     @"NameBgr"
#define SETTING_OUTPUTMODE              @"OUTPUTMODE"
#define SETTING_BADGOODDAY_KEY          @"BadGoodDay"

#define SETTING_MODETEMP_KEY            @"ModeTemp"
#define SETTING_ONOFFWEATHER_KEY        @"OnOffWeather"
#define SETTING_ONOFFNOTIFY_WEATHER     @"OnOffNotifyWeather"
#define SETTING_ONOFFNOTIFY_SPECIALDAY  @"OnOffNotifySpecialDay"
#define SETTING_ONOFFNOTIFY_EVENT       @"OnOffNotifyEvent"
#define SETTING_ONOFFNOTIFY_QUOTION     @"OnOffNotifyQuotion"
#define SETTING_SHOWHOUR_ONVIETNAM      @"SETTING_SHOWHOUR_ONVIETNAM"
#define SETTING_ONOFFEVENT_TET          @"OnOffEventTet"
#define SETTING_ONOFFEVENT_UNIVERSAL    @"OnOffEventUniversal"
#define SETTING_ONOFFEVENT_FESTIVAL     @"OnOffEventFestival"
#define SETTING_MODEGENDER_KEY          @"ModeGender"
#define SETTING_MODELOCATIONWEATHER_KEY          @"LocationWeather"
#define SETTING_MODE_LOCATION_ADDRESS_KEY          @"LocationAddress"

#define KEY_MONTH_USRER   @"Month_User"
#define KEY_YEAR_USRER      @"Year_User"
#define KEY_DAY_USRER   @"Day_User"

#define KEY_EVENT_BIRTHDAY @"Event_Birthday"
#define KEY_EVENT_MY_BIORHYTHM @"Event_My_Biorhythm"

//Define COLOR

#define COLOR_LINE                  RGB(204,204,204)
#define COLOR_LINE_SETTING          RGB(229,229,229)
#define COLOR_BTN_TABBAR_FOCUS      RGB(1,46,128)
#define COLOR_BTN_TABBAR_NORMAL      [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.4]

#define FONT_ROBOTO_ITALIC        @"Roboto-Italic"
#define FONT_ROBOTO_MEDIUM      @"Roboto-Medium"
#define FONT_ROBOTO_REGULAR     @"Roboto-Regular"
#define FONT_ROBOTO_LIGHT       @"Roboto-Light"

#define kHomeCountDayViewController                 @"HomeCountDayViewController"
#define kListCreatedEventCountDayViewController     @"ListCreatedEventCountDayViewController"
#define kListCreateNewEventViewController           @"ListCreateNewEventViewController"
#define kCreateEventViewController                  @"CreateEventViewController"
#define kResultCountDayViewController               @"ResultCountDayViewController"

#define FONT_SIZE_TITLE_IP4         19.2
#define FONT_SIZE_TITLE_IP5         22
#define FONT_SIZE_TITLE_IP6         23
#define FONT_SIZE_TITLE_IP6PLUS     23
#define FONT_SIZE_TITLE_IPAD        35


#define FONT_SIZE_CONTENT_IP4      15
#define FONT_SIZE_TITLE_STYLE1_IP4 16
#define FONT_SIZE_CONTENT_IP5      16
#define FONT_SIZE_TITLE_STYLE1_IP5 18
#define FONT_SIZE_CONTENT_IP6      17
#define FONT_SIZE_TITLE_STYLE1_IP6 16
#define FONT_SIZE_CONTENT_IP6PLUS      18
#define FONT_SIZE_TITLE_STYLE1_IP6PLUS 19
#define FONT_SIZE_CONTENT_IPAD      23
#define FONT_SIZE_TITLE_STYLE1_IPAD 25

#define TEXT_LICHNGAY       @"Lịch ngày"
#define TEXT_LICHTHANG      @"Lịch tháng"
#define TEXT_DOINGAY        @"Đổi ngày"
#define TEXT_KHAMPHA        @"Khám phá"
#define TEXT_CAUHINH        @"Cấu hình"
#define TEXT_VANHOA         @"Văn hoá Việt"

#ifdef __IPHONE_6_0 // iOS6 and later
#   define UITextAlignmentCenter    NSTextAlignmentCenter
#   define UITextAlignmentLeft      NSTextAlignmentLeft
#   define UITextAlignmentRight     NSTextAlignmentRight
#   define UILineBreakModeTailTruncation     NSLineBreakByTruncatingTail
#   define UILineBreakModeMiddleTruncation   NSLineBreakByTruncatingMiddle
#   define UILineBreakByWordWrapping         NSLineBreakByWordWrapping
#endif


#define Font Defined
#define kFontContentItem            [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(15.f)]
#define kFontButtonDetail           [UIFont fontWithName:getFontName(MODE_FONT_MEDIUM,NO) size:GetFontSizeMaxFitIP6Plus(14.f)]
#define kFontTitleCard              [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(14.f)]
#define kFontTitleItem              [UIFont fontWithName:getFontName(MODE_FONT_MEDIUM,NO) size:GetFontSizeMaxFitIP6Plus(11.f)]
#define kFontContentParagraph       [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(15.f)]

#define kFontTitleSearchCard        [UIFont fontWithName:getFontName(MODE_FONT_REGULAR,NO) size:GetFontSizeMaxFitIP6Plus(13.f)]


enum {
    SMM_CONTACTUS = 0,
    SMM_TELLFRIENDS
}SSM_EMAIL;


enum OutputMode
{
	OPM_EUROPEAN = 0,
	OPM_ENGLISH_US = 1
};

enum DateTimeMode
{
    MODE_24H = 0,
    MODE_12H = 1
};

enum TempTypeMode
{
    MODE_TempC = 1,
    MODE_TempF = 0
};

enum GenderMode
{
    MODE_FEMALE = 1,
    MODE_MALE = 0,
    MODE_OTHER = 2
};

enum PHOTO_OPTION {
	PHOTO_RANDOM = 0,
	PHOTO_OWNER,
	PHOTO_LIBRARY
};

enum SHOWHIDE_HOURSTYPEVIET {
    SHOWHIDE_RANDOM = 0,
    SHOWHIDE_TRUE,
    SHOWHIDE_FALSE
};

enum MODE_FONT{
    MODE_FONT_REGULAR = 0,
    MODE_FONT_MEDIUM,
    MODE_FONT_BOLD,
    MODE_FONT_ITALIC,
    MODE_FONT_LIGHT
};

typedef enum {
    DEVICE_TYPE_IPHONE5 = 0,
    DEVICE_TYPE_IPHONE6,
    DEVICE_TYPE_IPHONE6_PLUS,
    DEVICE_TYPE_IPHONE,
    DEVICE_TYPE_IPAD
    
}DEVICE_TYPE;


typedef struct CONFIG_APP {
    //Lich ngay
    float fFontSizeTitleLichNgay;
    float fFontSizeThu;
    
    //Nice DayView
    float fSizeIconNiceDayView;
    float fLeftMarginNiceDayView;
    float fSpaceItemNiceDayView;
    
    //Setting
    float fHeightAdsBanner;
    float fHeightAdsRectangle;
    
    //General
    
    NSString *strPrefixImgDecript;
    NSString *strPrefixImg1;
    NSString *strPrefixImg2;
    
    
    float fFontSizeTitle;
    float fFontSizeTopBarHeader; //Top bar cac muc noi dung chinh
    float fFontSizeTopTabBar; // Top tab bar.
    float fFontArticleTitle; //Title cua noi dung cac phan
    float fFontSizeTextButton;
    float fFonsizeTextTabBar; //Text Tab bar: Le Hoi
    
    float fFontContent; //Content cua noi dung cac phan
    float fFontTitleLikeQuotion;
    float fFontSectionHeader;
    float fFontDetailTitle;
    float fHeightCaption;
    float fSpacing;
    float fHeightCellTableView;
    float fHeightHeaderBar;
    float fHeightBottomBar;
    float fHeightFrameDetailInfoDay; //Chieu cao cua phan hien thi thong tin lich ngay
    float fLeftMargin;
    float fWButton;
    
}CONFIG_APP;


enum ProvinceVN
{
    Province_Auto = 0,
    Province_BenTre,
    Province_CaoBang,
    Province_HaiPhong,
    Province_LaiChau,
    Province_LamDong,
    Province_LongAn,
    Province_QuangNam,
    Province_QuangNinh,
    Province_SonLa,
    Province_TayNinh,
    Province_ThanhHoa,
    Province_ThaiBinh,
    Province_TienGiang,
    Province_LangSon,
    Province_AnGiang,
    Province_Daclak,
    Province_DongNai,
    Province_DongThap,
    Province_KienGiang,
    Province_Hanoi,
    Province_HCM,
    Province_BaRiaVungTau,
    Province_BinhThuan,
    Province_CanTho,
    Province_GiaLai,
    Province_HaGiang,
    Province_HaTinh,
    Province_HoaBinh,
    Province_KhanhHoa,
    Province_LaoCai,
    Province_HaNam,
    Province_NgheAn,
    Province_NinhBinh,
    Province_NinhThuan,
    Province_PhuYen,
    Province_QuangBinh,
    Province_QuangTri,
    Province_SocTrang,
    Province_ThuaThienHue,
    Province_TuyenQuang,
    Province_VinhLong,
    Province_YenBai,
    Province_Kontum,
    Province_QuangNgai,
    Province_BinhDuong,
    Province_HungYen,
    Province_HaiDuong,
    Province_BacLieu,
    Province_CaMau,
    Province_ThaiNgyen,
    Province_Backan,
    Province_Dannang,
    Province_BinhPhuoc,
    Province_BacGiang,
    Province_BacNinh,
    Province_NamDinh,
    Province_VinhPhuc,
    Province_Phutho,
    Province_Dienbien,
    Province_DacNong,
    Province_BinhDinh,
    Province_HauGiang,
    Province_TraVinh,
    Province_Total
};


#pragma mark Variables
@class AppDelegate;
extern NSString* g_strMainBundlePath;
extern NSString* g_strQuoteCateName;
extern NSString* g_strDocumentPath;
extern NSMutableDictionary* g_pDictSpecialDay;
extern bool g_bOSVersionIsAtLeastiOS7;
extern NSMutableArray*	g_arrayCan;
extern NSMutableArray*	g_arrayChi;
extern BOOL g_bCallShowAds;
extern NSDate *g_pDateStartSwichView;

//extern BOOL g_bShowRate;
extern NSMutableArray *g_ArrIndexBgr;


extern UIFont *g_fontMenu;
extern NSMutableDictionary *g_pDictSetting;
extern NSDateFormatter *g_NSDateFormatter;

extern OutputMode  g_iOldModeOutPut;
extern BOOL         g_bAppIsInForeground;
extern bool g_bIsDaySpec;
extern int				g_iOldSelPhotoOption;
extern int  g_iCurModeShowAds;
extern int  g_iCountImgBgr;
extern float g_fSapceItem;
extern int g_nCurSendMailMode;
extern BOOL g_bCheckShowAdsFromPopUp;
extern BOOL g_bTapDetail;
extern BOOL g_bShowTapDetail;
extern BOOL g_bLocationUpdate;
extern  DEVICE_TYPE                  g_DeviceType;
extern CONFIG_APP g_stConfigApp;


typedef struct LVFloat{
    CGFloat iPhone;
    CGFloat iPhone5;
    CGFloat iPhone6;
    CGFloat iPhone6Plus;
    CGFloat iPad;
    CGFloat value(){
        switch(g_DeviceType){
            case DEVICE_TYPE_IPHONE:
            {
                return iPhone;
                break;
            }
            case DEVICE_TYPE_IPHONE5:
            {
                return iPhone5;
                break;
            }
            case DEVICE_TYPE_IPHONE6:
            {
                return iPhone6;
                break;
            }
            case DEVICE_TYPE_IPHONE6_PLUS:
            {
                return iPhone6Plus;
                break;
            }
            case DEVICE_TYPE_IPAD:
            {
                return iPad;
                break;
            }
            default:
            {
                return iPhone;
            }
                break;
        }
        
    };
    
}LVFloat;

CG_INLINE LVFloat
LVFloatMake(CGFloat iPhone, CGFloat iPhone5, CGFloat iPhone6, CGFloat iPhone6Plus, CGFloat iPad)
{
    LVFloat rect;
    rect.iPhone = iPhone;
    rect.iPhone5 = iPhone5;
    rect.iPhone6 = iPhone6;
    rect.iPhone6Plus = iPhone6Plus;
    rect.iPad = iPad;
    return rect;
}


//Check ios device
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#pragma mark Settig Actionss 
void loadSetting();
void saveSetting();
DEVICE_TYPE getTypeDivice(UIView *pView);
UIImage* getImage12ConGiap(int nChi);
UIImage* getImage12ConGiapNew(int nChi);
//NSString *GetInfoDay(CCoreLVN dat, CLunarDate ldat);
//void GetDayInfo(NSString* &strDayInfo, CCoreLVN dat, CLunarDate ldat);
bool checkFileExist(const char *szPath);
bool isGoodHour(int iCan, int iChi,NSString *sCurHour);
void setInfoTimeZone();
//CString getFormatDay(int nMin,int nHour);
//CString insertCharToString (CString sSrc,CString sSeperator);
UIImage* decriptImageDataInRsource(NSString *sPathImg,NSString*sNameImg);
void enCriptData();
CGSize getSizeText(NSString *sText, UIFont *font,float fWidth);
NSString* getDeviceTypeStoryboardName();
UIImage* getImageWithButton(UIButton* btn,UIColor *color);
UIImage* getImage12ConGiap(int nChi);
void setAttributedString(UILabel *label, NSString *text, NSTextAlignment textAlignment, CGFloat headerLineIndent);

//void sendUserProperties(NSString *property, NSString *value);
//void sendTrackEventValue(NSString *screenName, NSString *action,NSString *detail);
//void sendLoginTrackEventValue(NSString *screenName, NSString *action,NSString *detail);
//void sendShareTrackEventValue(NSString *screenName, NSString *action,NSString *detail);
//void sendTrackEventScreen(NSString *sNameCategory);
//void sendTrackCustomEventView(NSString *screenName);
//void sendTrackNativeAdsInView(NSString *screenName);
//

void setDateStartShowFullAds();
BOOL checkShowFullAds();
NSString *getFontName(MODE_FONT modeFont, BOOL isLargeText);
NSString *getHMSegmentedControlFontName(MODE_FONT modeFont, BOOL isLargeText);
void setAttributedString(UILabel *label, NSString *text, NSTextAlignment textAlignment);

extern AppDelegate* g_AppDelegate;
CGFloat GetBottomBarHeight();

#endif



static void animateView1(UIView* view ,NSString*transition,NSString *direction,NSTimeInterval duration){
    // Set up the animation
    CATransition *animation = [CATransition animation];
//    [animation setDelegate:view];
    // Set the type and if appropriate direction of the transition,
    if (transition == kCATransitionFade) {
        [animation setType:kCATransitionFade];
    } else {
        [animation setType:transition];
        [animation setSubtype:direction];
    }
    // Set the duration and timing function of the transtion -- duration is passed in as a parameter, use ease in/ease out as the timing function
    [animation setDuration:duration];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[view layer] addAnimation:animation forKey:@"animationTransition"];
    [[view layer] removeAnimationForKey:@"animationTransition"];
}


static void animateView(UIView* view ,NSString*transition,NSString *direction,NSTimeInterval duration, id delegate = nil, BOOL removeViewFromSupperview = NO){
    // Set up the animation
    CATransition *animation = [CATransition animation];
//    [animation setDelegate:delegate];
    
    if(removeViewFromSupperview)
        if([delegate respondsToSelector:@selector(setAnimatedView:)])
            [delegate performSelector:@selector(setAnimatedView:) withObject:view];
    
    // Set the type and if appropriate direction of the transition,
    if (transition == kCATransitionFade) {
        [animation setType:kCATransitionFade];
    } else {
        [animation setType:transition];
        [animation setSubtype:direction];
    }
    // Set the duration and timing function of the transtion -- duration is passed in as a parameter, use ease in/ease out as the timing function
    [animation setDuration:duration];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[view layer] addAnimation:animation forKey:@"animationTransition"];
    [[view layer] removeAnimationForKey:@"animationTransition"];
}

static void animateView13(UIView* view ,NSString*transition,NSString *direction,NSTimeInterval duration, id delegate = nil, BOOL removeViewFromSupperview = NO){
    //    if(removeViewFromSupperview){
    //        [view setHidden:YES];
    //    }
    // Set up the animation
    CATransition *animation = [CATransition animation];
    [animation setDelegate:delegate];
    
    if(removeViewFromSupperview){
        if([delegate respondsToSelector:@selector(setAnimatedView:)])
            [delegate performSelector:@selector(setAnimatedView:) withObject:view];
    }
    
    
    // Set the type and if appropriate direction of the transition,
    if (transition == kCATransitionFade) {
        [animation setType:kCATransitionFade];
    } else {
        [animation setType:transition];
        [animation setSubtype:direction];
    }
    // Set the duration and timing function of the transtion -- duration is passed in as a parameter, use ease in/ease out as the timing function
    [animation setDuration:duration];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[view layer] addAnimation:animation forKey:@"animationTransition"];
    [[view layer] removeAnimationForKey:@"animationTransition"];
    
    if(removeViewFromSupperview){
        if([delegate respondsToSelector:@selector(removeAnimatedViewFromSuperview)])
            [delegate performSelector:@selector(removeAnimatedViewFromSuperview) withObject:nil afterDelay:duration*0.9];
    }
}




static UIImage* UIImageByResizeImage(UIImage* image, CGSize newSize){
    if(image == nil)
        return nil;
    
    
    float fScale = 1.0;
    
    if([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0 )
    {
        fScale = 2.0;
    }
    
    
    CGContextRef			context;
    CGColorSpaceRef			imageColorSpace;
    
    newSize.width *= fScale;
    newSize.height *= fScale;
    
    //Create our colorspaces
    imageColorSpace = CGColorSpaceCreateDeviceRGB();
    //Resize the puzzle image
    context = CGBitmapContextCreate(NULL, newSize.width, newSize.height, 8, 0, imageColorSpace, kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(imageColorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, newSize.width, newSize.height), image.CGImage);
    CGImageRef cgImg = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
   
    
    UIImage* resultImage = nil;
    if (fScale == 2.0 && [UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)])
        resultImage =  [UIImage imageWithCGImage:image.CGImage scale:fScale orientation:UIImageOrientationUp];
    else
        resultImage = [UIImage imageWithCGImage:image.CGImage];
    
    
     CGImageRelease(cgImg);
    return resultImage;

    
    /*
    if(fScale == 2.0)
        resultImage = [[UIImage alloc] initWithCGImage:cgImg scale:fScale orientation:UIImageOrientationUp];
    else
        resultImage = [[UIImage alloc] initWithCGImage:cgImg];
    CGImageRelease(cgImg);
    
    return resultImage;
     */
}

#pragma mark Iterface Utilites
@interface UIXImage : UIImage
@end

@interface UIImage (extended)
+ (UIImage*)resourceImageNamed:(NSString *)name;
+ (UIImage*)imageInResourcePath:(NSString*)path named:(NSString *)name;
@end

#pragma mark CustomButtonText
@interface UITextButton: UIButton
{
    UILabel *m_pLbl1;
    UILabel *m_pLbl2;
}

- (void) setFontControl:(UIFont*)pFont;
- (void) setText:(NSString*)sText1 withLine2:(NSString*)sText2;
- (void) setTextButtonColor:(UIColor*)clrText;
@end

#pragma mark CustomButtonText
@interface UIBarButton: UIButton
{
    UILabel *m_pLbl1;
    UILabel *m_pLblNum;
    UILabel *notiLabel;
    UIImageView *m_pImgView;
}
- (void) updateNotificationLabelWithNumberNoti:(int) numberNoti;
- (void) setFontControl:(UIFont*)pFont;
- (void) setTextButton:(NSString*)aText andColor:(UIColor*)clr;
- (void) setNum:(NSString*)aText;
- (void) setImageButton:(UIImage*)pImg;

UIImage     *MakeImage(UIColor *color, CGRect bounds);
UIImage     *MakeHighlightedImage(UIColor *color, CGRect bounds);
UIImage *MakeEventGoogleIcon(UIColor *color, NSString *text , UIFont *font, CGRect bounds);
UIImage *DrawTextImage(CGRect bounds, UIColor *backgroundColor, CGFloat radius, UIColor *borderColor, CGFloat borderWidth, NSString *text, UIColor *textColor, UIFont *font);

NSString    *NSStringFormatted(NSString *string, NSString *separateString);
UIImage     *CaptureScreenFromView(UIView *view);

@end

@interface UIImage (YALExtension)
+ (UIImage *)imageWithRoundedCorners:(CGFloat)cornerRadius size:(CGSize)sizeToFit
                           fillColor:(UIColor*)fillColor
                         strokeColor:(UIColor*)strokeColor
                           lineWidth:(CGFloat)lineWidth;
@end


@interface UIImage (averageColor)
- (UIColor *)averageColor;
- (UIColor *)averageDarkColor;

@end


