//
//  MVGlobal.h
//  MyViettel
//
//  Created by Nguyen Minh Hai on 6/16/15.
//  Copyright (c) 2015 PPCINK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>

#ifdef __cplusplus
extern "C"
{
#endif

#pragma mark - Classes

#pragma mark - Enums
typedef NS_ENUM(NSInteger, LVScreenSize) {
    LVScreenSizeIp4 = 2000,
    LVScreenSizeIp5,
    LVScreenSizeIp6,
    LVScreenSizeIp6Plus,
    LVScreenSizeIpad
};

#pragma mark - Variables
extern LVScreenSize gScreenSize;
extern NSInteger gIOSVersion;
extern CGFloat gScreenWidth;
extern CGFloat gScreenHeight;
extern BOOL    bNeedDownloadPromotion;
extern BOOL    bNeedDownloadNews;

#pragma mark - Functions
    
float   GetSizeHeaderBar();
float   GetSizeQuotationDaysView();
//CGSize  GetSizeForText(NSString *text, UIFont *font, CGSize boundingSize);
float   GetFontSizeFitForIP6Plus(float size);
float   GetFontSizeMaxFitIP6Plus(float size);
float GetSpaceBetweenItemForIphone6Plus(float space, float delta);
//void    DrawRectView(UIView *view);
//void    DrawShadowLabel(UILabel *label);
//NSString *ConvertNSDateToString(NSDate *date);
//
//CGFloat MakeDistanceValue(CGFloat IP4Distance, CGFloat IP5Distance, CGFloat IP6Distance, CGFloat IP6PlustDis, CGFloat IPadDistance);
//UIImage *LVBackgroundImage();
//UIImage* ResizeImageWithImage(UIImage* sourceImage, float newHeight);

#ifdef __cplusplus
};
#endif

