//
//  main.m
//  PPCLINKLibs_Sample
//
//  Created by Do Lam on 5/27/15.
//  Copyright (c) 2015 Do Lam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
