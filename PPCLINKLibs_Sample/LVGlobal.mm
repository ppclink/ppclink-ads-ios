//
//  MVGlobal.m
//  MyViettel
//
//  Created by Nguyen Minh Hai on 6/16/15.
//  Copyright (c) 2015 PPCINK. All rights reserved.
//

#import "LVGlobal.h"
//#import "NSDate+Utilities.h"

#pragma mark - Classes
#pragma mark - Global variables
LVScreenSize gScreenSize;
NSInteger gIOSVersion;
CGFloat gScreenWidth;
CGFloat gScreenHeight;
NSArray* g_localContacts = nil;
BOOL    bNeedDownloadPromotion = NO;
BOOL    bNeedDownloadNews = YES;

#pragma mark - Functions
#pragma mark ==Utilities

float GetSizeHeaderBar(){
    if (gScreenSize == LVScreenSizeIp6Plus) {
        return 64.f;
    }
    if (gScreenSize == LVScreenSizeIp6) {
        return 64.f;
    }
    if (gScreenSize == LVScreenSizeIp5) {
        return 64.f;
    }
    if (gScreenSize == LVScreenSizeIp4) {
        return 64.f;
    }
    return 104.f;
}

//float GetSizeQuotationDaysView(){
//    return gScreenWidth+20;
//}

//CGSize GetSizeForText(NSString *text, UIFont *font, CGSize boundingSize) {
//    UILabel *gettingSizeLabel = [[UILabel alloc] init];
//    gettingSizeLabel.font = font;
//    gettingSizeLabel.text = text;
//    gettingSizeLabel.numberOfLines = 0;
//    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    CGSize maximumLabelSize = boundingSize;
//    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
//    return expectSize;
//}

float GetFontSizeFitForIP6Plus(float size){
    if (gScreenSize == LVScreenSizeIp4 || gScreenSize == LVScreenSizeIp5) {
        return size - 2;
    }else if (gScreenSize == LVScreenSizeIp6){
        return size - 1;
    }else if (gScreenSize == LVScreenSizeIpad){
        return ceilf(size * 1.5f);
    }
    return size;
}

float GetFontSizeMaxFitIP6Plus(float size){
    if (gScreenSize == LVScreenSizeIpad){
        return ceilf(size * 1.5f);
    }
    return size;
}

//CGFloat MakeDistanceValue(CGFloat IP4Distance, CGFloat IP5Distance, CGFloat IP6Distance, CGFloat IP6PlustDis, CGFloat IPadDistance){
//    if (gScreenSize == LVScreenSizeIp4) {
//        return IP4Distance;
//    }
//    if (gScreenSize == LVScreenSizeIp5) {
//        return IP5Distance;
//    }
//    if (gScreenSize == LVScreenSizeIp6) {
//        return IP6Distance;
//    }
//    if (gScreenSize == LVScreenSizeIp6Plus) {
//        return IP6PlustDis;
//    }
//    return IPadDistance;
//}

float GetSpaceBetweenItemForIphone6Plus(float space, float delta){
    if (gScreenSize == LVScreenSizeIp4 || gScreenSize == LVScreenSizeIp5) {
        return (space - 2 * delta);
    }else if (gScreenSize == LVScreenSizeIp6){
        return (space - delta);
    }else if (gScreenSize == LVScreenSizeIp6Plus){
        return space;
    }
//    if (gScreenSize == LVScreenSizeIpad) {
    return ceilf(space * 1.5f);
//    }
//    return 16.f;
    
}

//void DrawRectView(UIView *view){
//    view.layer.cornerRadius = GetFontSizeFitForIP6Plus(7.f);
//    view.layer.masksToBounds = YES;
//}
//void DrawShadowLabel(UILabel *label){
//    label.layer.shadowColor = [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:1].CGColor;
//    label.layer.shadowOpacity = 0.6;
//    label.layer.shadowRadius = 4.f;
//    label.layer.shadowOffset = CGSizeMake(0.f, 3.f);
//}
//
//NSString *ConvertNSDateToString(NSDate *date){
//    return [NSString stringWithFormat:@"%ld-%ld-%ld",date.year, date.month, date.day];
//}
//
//UIImage *LVBackgroundImage(){
////    return [[UIImage imageNamed:@"card_bgr_boder"] resizableImageWithCapInsets:UIEdgeInsetsMake(20.f, 20.f, 20.f, 20.f)];
//    return [[UIImage imageNamed:@"bgr_boder1"] resizableImageWithCapInsets:UIEdgeInsetsMake(15.f, 20.f, 20.f, 20.f)];
//}
//
//UIImage* ResizeImageWithImage(UIImage* sourceImage, float newHeight)
//{
//    CGFloat scale = [[UIScreen mainScreen] scale];
//    float oldHeight = sourceImage.size.height;
//    float scaleFactor = newHeight / oldHeight;
//    
//    float iWidth = sourceImage.size.width * scaleFactor;
//    float iHeight = oldHeight * scaleFactor;
//    
//    UIGraphicsBeginImageContextWithOptions(CGSizeMake(iWidth, iHeight), NO, 0.0);
//
////    UIGraphicsBeginImageContext(CGSizeMake(iWidth*scale, iHeight*scale));
//    [sourceImage drawInRect:CGRectMake(0, 0, iWidth, iHeight)];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return newImage;
//}
