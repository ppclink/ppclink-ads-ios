//
//  AppDelegate.h
//  PPCLINKLibs_Sample
//
//  Created by Do Lam on 5/27/15.
//  Copyright (c) 2015 Do Lam. All rights reserved.
//

#import <UIKit/UIKit.h>
//@import FirebaseRemoteConfig;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

